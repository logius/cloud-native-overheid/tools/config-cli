package commands

import (
	"fmt"
	"io/ioutil"
	"log"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	gitlab "gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/config"
	grafana "gitlab.com/logius/cloud-native-overheid/tools/grafana-cli-v2/config"
	harbor "gitlab.com/logius/cloud-native-overheid/tools/harbor-cli/config"
	kibana "gitlab.com/logius/cloud-native-overheid/tools/kibana-cli/config"
	mattermost "gitlab.com/logius/cloud-native-overheid/tools/mattermost-cli/config"
	nexus "gitlab.com/logius/cloud-native-overheid/tools/nexus-cli/config"
	rancher "gitlab.com/logius/cloud-native-overheid/tools/rancher-cli/config"
	"gopkg.in/yaml.v3"
)

type flags struct {
	config *string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return validateConfig(cmd, &flags)
		},
		Use:   "validate-config",
		Short: "Validate",
		Long:  "Validate config file",
	}

	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")

	cmd.MarkFlagRequired("config")

	return cmd
}

// use a single instance of Validate, it caches struct info
var validate *validator.Validate

func validateConfig(cmd *cobra.Command, flags *flags) error {

	validate = validator.New()

	products, err := getProducts(*flags.config)
	if err != nil {
		return err
	}

	cliValidators := make(map[string]interface{})

	for _, product := range products {
		var configError error

		switch product {
		case "gitlab":
			configData, err := gitlab.LoadConfigfile(*flags.config)
			configError = err
			cliValidators["gitlab"] = configData
		case "grafana":
			configData, err := grafana.LoadConfigfile(*flags.config)
			configError = err
			cliValidators["grafana"] = configData
		case "harbor":
			configData, err := harbor.LoadConfigfile(*flags.config)
			configError = err
			cliValidators["harbor"] = configData
		case "kibana":
			configData, err := kibana.LoadConfigfile(*flags.config)
			configError = err
			cliValidators["kibana"] = configData
		case "mattermost":
			configData := mattermost.LoadConfigfile(*flags.config)
			configError = err
			cliValidators["mattermost"] = configData
		case "nexus":
			configData, err := nexus.LoadConfigfile(*flags.config)
			configError = err
			cliValidators["nexus"] = configData
		case "rancher":
			configData, err := rancher.LoadConfigfile(*flags.config)
			configError = err
			cliValidators["rancher"] = configData
		}
		if configError != nil {
			return err
		}
	}

	failed := false
	for cli, customerConfig := range cliValidators {

		err := validate.Struct(customerConfig)
		if err != nil {
			for _, err := range err.(validator.ValidationErrors) {
				failed = true
				log.Println(cli, "validation failed")
				fmt.Println(err)
				time.Sleep(500 * time.Millisecond)
			}

		} else {
			log.Println(cli, "validation passed")
			time.Sleep(500 * time.Millisecond)
		}
	}
	if failed {
		return fmt.Errorf("errors in configuration")
	}
	return nil
}

func getProducts(configFile string) ([]string, error) {
	yamlFile, err := ioutil.ReadFile(configFile)
	if err != nil {
		return nil, errors.Errorf("Error reading YAML file: %s\n", err)
	}

	data := make(map[string]map[string]interface{})

	err = yaml.Unmarshal(yamlFile, &data)

	if err != nil {
		return nil, errors.Errorf("Error reading YAML file: %s\n", err)
	}

	customer := data["customer"]
	if customer == nil {
		return nil, errors.Errorf("Mising 'customer' tag in YAML file")
	}

	keys := make([]string, 0, len(customer))
	for k := range customer {
		keys = append(keys, k)
	}

	return keys, nil
}
