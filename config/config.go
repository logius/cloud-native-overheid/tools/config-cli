package config

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v3"
)


// LoadConfigfile reads config from YAML
func LoadConfigfile(fileName string) (interface{}) {
	var config interface{}
	yamlFile, err := ioutil.ReadFile(fileName)
	if err != nil {
		return fmt.Errorf("error reading YAML file: %s\n", err)
	}

	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		return fmt.Errorf("error parsing YAML file: %s\n", err)
	}
	return config
}
