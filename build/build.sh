#
# Run this script from the root of the project so it will load from the proper context. 
#
export DOCKER_BUILDKIT=1
docker build -f ./build/Dockerfile -t registry.gitlab.com/logius/cloud-native-overheid/tools/config-cli:local .
