#!/bin/bash

TOOLBOX_IMG=registry.gitlab.com/logius/cloud-native-overheid/tools/config-cli:local

docker run --rm \
  -v $PWD/examples:/examples \
  $TOOLBOX_IMG validate-config --config=/examples/test.config.yml


