module gitlab.com/logius/cloud-native-overheid/tools/config-cli

go 1.17

replace gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli => gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli-v2 v0.0.0-20210928115127-58686dad1105

replace k8s.io/client-go => k8s.io/client-go v0.20.4 // derived from rancher-cli

require (
	github.com/go-playground/validator/v10 v10.9.0
	github.com/spf13/cobra v1.2.1
	gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli v0.0.0-20210409120209-2cf55852f3a4
	gitlab.com/logius/cloud-native-overheid/tools/grafana-cli-v2 v0.0.0-20211004125145-0e87c840eafa
	gitlab.com/logius/cloud-native-overheid/tools/harbor-cli v0.0.0-20211004125129-bd0ad36a94d2
	gitlab.com/logius/cloud-native-overheid/tools/kibana-cli v0.1.3-0.20211005131742-61de4040af94
	gitlab.com/logius/cloud-native-overheid/tools/mattermost-cli v0.0.0-20211004124902-e7fb851b7dc8
	gitlab.com/logius/cloud-native-overheid/tools/nexus-cli v0.0.0-20211005182215-16544ecd1d47
	gitlab.com/logius/cloud-native-overheid/tools/rancher-cli v0.0.0-20211004135048-21988fe7d257
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

require (
	github.com/go-playground/locales v0.14.0 // indirect
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97 // indirect
	golang.org/x/sys v0.0.0-20210806184541-e5e7981a1069 // indirect
	golang.org/x/text v0.3.6 // indirect
)
