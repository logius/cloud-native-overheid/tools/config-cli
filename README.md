## config - CLI ##
Validates the customer config against the structs defined in the CLIs fetched in /commands/command.go
Provides detailed feedback, e.g.: 

    2021/09/30 10:09:36 nexus validation passed
    2021/09/30 10:09:36 harbor validation passed
    2021/09/30 10:09:36 grafana validation passed
    2021/09/30 10:09:36 mattermost validation passed
    2021/09/30 10:09:36 kibana validation passed
    Key: 'Config.Customer.GitLab.Groups' Error:Field validation for 'Groups' failed on the 'required' tag
    2021/09/30 10:09:36 gitlab validation failed

Config-CLI uses the go-playground validator, for documentation see:
https://github.com/go-playground/validator
https://pkg.go.dev/github.com/go-playground/validator/v10

Config-CLI is invoked on every change to the customer config.
Therefore every customer config project contains a .gitlab-ci that includes validate-config.yml from lpc-platform-bridges.
See customer LPCtest for an example of the .gitlab-ci.

